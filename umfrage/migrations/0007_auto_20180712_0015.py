# Generated by Django 2.0.7 on 2018-07-11 19:45

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('umfrage', '0006_auto_20180711_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='frage',
            name='id',
            field=models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False),
        ),
    ]
