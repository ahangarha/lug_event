from django.shortcuts import render,redirect,get_object_or_404
from django.views import generic
from .models import Frage
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import json
from django.http import HttpResponseForbidden ,HttpRequest
# Create your views here.

def main(request):
    return render(request , 'main.html')

def detail(request , pk):  # needs to write by  genericview and form 

    frage = get_object_or_404(Frage , id=pk)
    dic = json.loads(frage.opts)


    if request.method == 'POST':
        x= request.session.get(request.get_full_path())
        if 'remove' in request.POST :
            for key in x:
                dic[key] -= 1
            frage.opts= json.dumps(dic)
            frage.save()
            try:
                del request.session[request.get_full_path()]
            except KeyError:
                pass
            return render(request, 'detail.html',{'frage':frage,'dic':dic})
        if x is not None:
            return render(request, 'detail.html',{'frage':frage,'dic':dic,'voted':x })

        check_list = request.POST.getlist('checks')
        for key in check_list:
            dic[key] += 1
        frage.opts = json.dumps(dic)
        frage.save()
        request.session[request.get_full_path()] = check_list 
        return render(request, 'detail.html',{'frage':frage,'dic':dic,'path':request.get_full_path()})


    else:
        return render(request, 'detail.html',{'frage':frage,'dic':dic})


@login_required
def profile(request):
    fragen = Frage.objects.filter(user = request.user)

    return render(request , 'profile.html',{'fragen':fragen})

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('profile')
        return render(request,'login.html',{'error':True,})

    else:
        return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('/')

@login_required
def add_new(request):
    if request.method =='POST':
        opts_tmp  = request.POST["opts"]
        opts_tmp = opts_tmp.split(';')
        opts_tmp = {k: 0 for k in opts_tmp }
        opts_tmp = json.dumps(opts_tmp)
        new_frage = Frage(name = request.POST['name'] , platz = request.POST['platz'],opts = opts_tmp , user = request.user)
        new_frage.save()
        return redirect('profile')

    else:
        return render(request , 'add_event.html')


def new_user_view(request):
    if request.method == 'POST':
        try:
            new_user= User.objects.get(username= request.POST["username"])
        except User.DoesNotExist:
            new_user = User(username = request.POST["username"])
            new_user.set_password(request.POST["password"])
            new_user.save()
            return redirect('login')
        else:
            return render(request , 'newuser.html',{'error':True})
    else:
        return render(request , 'newuser.html')

@login_required
def remove(request , pk):
    frage = get_object_or_404(Frage , id=pk)
    if request.method == 'POST':
        if( frage.user == request.user ):
            frage.delete()
            return redirect('profile')
        else:
            return HttpResponseForbidden()
    else:
        return render(request,'remove.html',{'frage':frage})


@login_required
def edit(request,pk):
    frage = get_object_or_404(Frage , id=pk)
    if( frage.user == request.user ):
        dic = json.loads(frage.opts)
        if request.method =='GET':
            li = []
            for k,v in dic.items():
                li.append(k)
            li = ';'.join(li)
            return render(request ,'edit.html',{'frage':frage,'li':li})
        else:
            opts_tmp  = request.POST["opts"]
            opts_tmp = opts_tmp.split(';')
            opts_tmp = {k: 0 for k in opts_tmp }
            opts_tmp = json.dumps(opts_tmp)
            frage.opts = opts_tmp
            frage.platz = request.POST['platz']
            frage.name = request.POST['name']
            frage.save()
            return redirect('profile')
    else:
            return HttpResponseForbidden()



class FrageListView(generic.ListView):
    model = Frage



